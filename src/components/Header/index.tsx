// import { ROUTE_PATHS } from '../../config/routes';
// import { useNavigate } from "react-router-dom";
import { Head, Navigator } from "./style";
import logo from "../../assets/logooi2.png";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { Button } from "../../styles/global";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
  import { faRightToBracket } from '@fortawesome/free-solid-svg-icons';
  import { faHeadset } from '@fortawesome/free-solid-svg-icons';

  import {Icones} from "./style"

export default function Header() {
   let navigate = useNavigate();
  const [name, setName] = useState("");

  useEffect(() => {
    setName(localStorage.getItem("name") || "");
  }, []);

  function logout() {
    localStorage.clear();
    navigate("/");
  }

  return (
    <div className="header">
      <Head>
       <Link to="/campaign-create">
          <img src={logo} alt="" />
        </Link> 
        <Navigator>
        <Link to="/campaign-create">Campanhas</Link>
        <Link to="/management">Gerenciamento</Link>
        {/* <Link to="/black-list">Black List</Link> */}
        <Link to="/user-interface">Cadastro de Usuário</Link> 
        {/* <Link to="/logs">Log</Link>
        <Link to="/customer-response">Resposta</Link> */}
        </Navigator>
        <span>{name}</span>
        <Link to="/suporte"> <FontAwesomeIcon style={{color: "white",height: "30px"}} icon={faHeadset}/></Link> 
        
         
        <Icones className="teste">
          <FontAwesomeIcon icon={faRightToBracket}  onClick={logout} />

        </Icones>
        {/* <Button className="fa-solid fa-right-from-bracket" onClick={logout}>  <FontAwesomeIcon icon={faRightToBracket}  onClick={logout} /></Button> */}
        {/* <FontAwesomeIcon icon="fa-solid fa-right-from-bracket" /> */}
      </Head>
    </div>
  );
}
