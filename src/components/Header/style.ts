import styled from "styled-components";

export const Head = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;

  height: 10vh;

  background-color: #667470;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
  padding: 0 150px;

  span {
    font-size: 20px;
    color: #ffffff;
  }

  img {
    width: 160px;
    height: 150px;
    cursor: pointer;
  }
`;



export const Icones = styled.div`
font-size: 1.5rem;
font-weight: bold;
border-radius: 5px;
outline: none;
cursor: pointer;
transition: 0.2s;
color:white;
background-color: #667470;


:hover {
  opacity: 0.85;
}

`

export const Navigator = styled.nav`
  display: flex;
  justify-content: space-around;

  width: 70%;

  a {
    font-size: 25px;
    color: #ffffff;
    text-decoration: none;
  }
`;
