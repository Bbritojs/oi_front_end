import styled from "styled-components";

export const Container = styled.div`
  background-color: #50AF31;
  height: 3vh;
  width: 100%;
  position: fixed;
  left: 0;
  bottom: 0;
`;
