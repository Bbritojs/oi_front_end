import axios from "axios";
import { User } from "../../screen/UserInterface/domain/user";
import { Ticket } from '../../screen/Suporte/domain/ticket';

const api = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

// interface AuthCredentials {
//   accessToken: string;
//   user: {
//     id: number;
//     email: string;
//   };
// }
function getCampaign() {
  return api.get("/campaign", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getCampaignBlackList(date : string) {
  return api.get(`/campaign/blacklist?date=${date}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getcampaignprogress() {
  return api.get("/campaign/progress", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getcampaignfulfilled() {
  return api.get("/campaign/fulfilled", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function createCampaign(campaign: FormData) {
  return api.post("/campaign", campaign, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function createCampaignCSV(campaign: FormData) {
  return api.post("/campaign/csv", campaign, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function createUser(user: User) {
  return api.post("/user", user, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getDelivery() {
  return api.get("/dynamo/status", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getOverview() {
  return api.get("/dynamo/sentAndFailed", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getSmsReceivedCsvExport(myDate: any) {
  return api.post(`dynamo/inbound-csv-export`, myDate,{
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function getWebhookStatusCsvExport(myDate: any) {
  return api.post(`dynamo/csv-export`, myDate,{
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function sendEmail(ticket: Ticket) {
  return api.post(`suporte/createEmail`, ticket,{
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

function login(email: string, password: string) {
  const data = {
    email,
    password,
  };
  return api.post("/auth/login", data);
}

export const brokerApi = {
  sendEmail,
  getcampaignprogress,
  getWebhookStatusCsvExport,
  createCampaignCSV,
  getCampaign,
  getCampaignBlackList,
  login,
  createUser,
  getDelivery,
  getSmsReceivedCsvExport,
};
