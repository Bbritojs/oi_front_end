export function getDate() {
  let dateObj = new Date();
  let month = dateObj.getUTCMonth() + 1;
  let day = dateObj.getUTCDate();
  let year = dateObj.getFullYear()%100;
  let actualDate = year + "-" + month + "-" + day;

  return actualDate;
}
