import styled from "styled-components";

export const AppDiv = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap");

  font-family: "Roboto", sans-serif;
`;

export const Button = styled.button`
  background-color: #00D31B;
  font-size: 1.5rem;
  font-weight: bold;
  border: 2px solid #00D31B;
  border-radius: 5px;
  outline: none;
  cursor: pointer;
  transition: 0.2s;


  :hover {
    opacity: 0.85;
  }
`;
