import { brokerApi } from "../../service/http-out/brokerApi";
import jwtDecode from "jwt-decode";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { Ticket } from "./domain/ticket";
import {
  Container,
  Title,
  InputContainer,
  Input,
  Select,
  LocalButton as Button,
} from "./style";

function SuporteCreate() {
  const ticket: Ticket = { email: "", name: "", subject: "", message: "" };
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [subject, setSubject] = useState("");

  async function handleClick() {
      ticket.name = name;
      ticket.email = email;
      ticket.subject = subject;
      ticket.message = message;

    await brokerApi
      .sendEmail(ticket)
      .then((response) => {
        console.log("resp ", response.data);
        reset();
        alert("Email enviado com sucesso")
      })
      .catch((error) => {
        console.log(error.response);
        alert("Ocorreu um erro ao enviar o email");
      });
    // setLoading(false);
  }

  function reset() {
    setName("")
    setEmail("")
    setMessage("")
  }

  return (
    <Container>
      <Title>Suporte ao usuário</Title>
      {/* <ContainerBoder> */}
      <InputContainer autoComplete="off">
        <Input 
        value={name} 
        onChange={(e) => setName(e.target.value)}
        type="text" placeholder="Nome completo"></Input>
        <Input 
        value={email} 
        onChange={(e) => setEmail(e.target.value)}
        type="text" placeholder="Seu e-mail"></Input>
        <Select  placeholder="Seu e-mail" onChange={(e) => {setSubject(e.target.value)}}>
            <option> Selecione o assunto </option>
            <option>Envio de SMS via API</option>
            <option>Envio de SMS via Portal</option>
            <option>Relatórios</option>
            <option>Acesso</option>
            <option>Dúvidas do Portal</option>
            <option>Outros</option>
        </Select>
        <Input style={{height: "100px",border: "10px solid #black"}}
        value={message}
        onChange={(e) => setMessage(e.target.value)}
        type="text" placeholder="Mensagem"></Input>
        <Button onClick={handleClick}>Enviar</Button>
      </InputContainer>
      {/* </ContainerBoder> */}
    </Container>
  );
}

export default SuporteCreate;
