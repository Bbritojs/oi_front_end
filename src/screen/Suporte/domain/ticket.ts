export interface Ticket {
    email: string;
    name: string;
    subject: string;
    message: string;
  }
  