import { brokerApi } from "../../service/http-out/brokerApi";
import { Campaign } from "../Campaign/domain/campaign";
import {
  Container,
  Title,
  InputContainer,
  InputFile,
  LabelFile,
  LocalButton as Button,
} from "./style";
import React, { useEffect, useState } from "react";
import { getDate } from '../../utils/functions'
import jwtDecode from "jwt-decode";
import { useNavigate } from "react-router";


function CampaingCreate(this: any) {
  const formData = new FormData();
  const [file, setFile] = useState("")
  const camp: Campaign = { file: {}, date: "", user: ""};
  function handleCsv(param: any) {
    setFile(param.target.files[0])
  }
  const navigate = useNavigate();

  useEffect(() => {
      const token = localStorage.getItem('token') || "";
      const decoded: any = jwtDecode(token);
      if (decoded.exp < Date.now() / 1000) {
        navigate("/");
      }
  });

  async function handleClick() {
    camp.date = getDate();
    camp.user = localStorage.getItem("id") || "";

    formData.append("date", camp.date);
    formData.append("user", camp.user);
    formData.append("file", file);

    await brokerApi
      .createCampaignCSV(formData)
      .then((response) => {
        formData.append("date", "");
        formData.append("user", "");
        console.log("resp ", response.data);
        reset();
        navigate("/campaign-create")
        alert("Campanha criada com sucesso")
      })
      .catch((error) => {
        console.log(error.response);
        reset();
        alert("Ocorreu um erro ao criar campanha");
      });
      navigate("/campaign-create");
  }

  function reset() {
    setFile("")
  }

  return (
    <Container>
      <Title>Criar Campanha</Title>
      <InputContainer>
        <LabelFile htmlFor="file">
          <i className="fas fa-arrow-up"></i>Fazer upload do arquivo .csv
        </LabelFile>
        <InputFile onChange={handleCsv} id="file" />
        <Button onClick={handleClick}>Criar Campanha</Button>
      </InputContainer>
    </Container>    
  );
}

export default CampaingCreate;
