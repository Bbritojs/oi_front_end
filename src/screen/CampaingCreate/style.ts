import styled from "styled-components";
import { Button } from "../../styles/global";
//#EADA22
//#00D31B
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 95%;
  height: 20vh;
  padding: 2rem 0 0 2rem;
`;

export const Title = styled.h1`
  color: #00000;
`;

export const InputContainer = styled.div`
  display: flex;
  width: 90vw;
  justify-content: space-between;
  align-items: center;
`;

export const Input = styled.input`
  padding: 10px;
  font-size: 25px;

  width: 40%;

  outline: none;
  border: 2px solid #d9d9d9;

  ::placeholder {
    color: #00000;
  }
`;

export const LabelFile = styled.label`
  display: flex;
  align-items: center;
  justify-content: space-around;

  width: 20%;
  padding: 10px;

  font-size: 25px;
  border: 2px solid #d9d9d9;
  color: #00000;
  cursor: pointer;
`;

export const InputFile = styled.input.attrs({ type: "file" })`
  display: none;
`;

export const ToggleContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  width: 15%;
  height: 100%;

  label {
    color: #00000;
    font-weight: bold;
  }
`;

export const ToggleButton = styled.input.attrs({ type: "checkbox" })`
  position: relative;

  width: 35%;
  height: 50%;

  -webkit-appearance: none;
  background: #00D31B;
  outline: none;
  border-radius: 25px;
  box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
  transition: 0.5s;


  :checked {
    background: #52B75B;
  }

  &:before {
    content: "";
    position: absolute;
    width: 45%;
    height: 100%;
    border-radius: 20px;
    top: 0;
    left: 0;
    background: #fff;
    transform: scale(1.1);
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
    transition: 0.5s;
   
  }

  :checked&:before {
    left: 60%;
  }
`;

export const LocalButton = styled(Button)`
  width: 17%;
  height: 100%;
  color: white
`;
