import styled from "styled-components";

export const Container = styled.div`
  background-color: #7b00cb;
  height: 3vh;
  width: 100%;
  position: fixed;
  left: 0;
  bottom: 0;
`;
