import styled from "styled-components";
import PasswordIcon from '@mui/icons-material/Password';

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    height: 100vh;

    background-color: #FAFAFA;
`

export const Main = styled.main`
    display: flex;
    justify-content: center;

    width: 25%;
    height: 78%;
    max-height: 660px;

    padding: 0em 1.5em 2em;
    border-radius: 0.5em;

    box-shadow: rgba(99, 99, 99, 0.4) 0px 2px 8px 0px;
`

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  /* justify-content: space-around; */

  width: 80%;


  span {
    color: #fff;
    font-size: 1.3rem;
    cursor: pointer;
  }
`;

export const Logo = styled(PasswordIcon)``

export const Title = styled.h3`
    color: #000;
`

export const Text = styled.p`
  color: #8E8E8E;
  text-align: center;

`
export const Label = styled.label`
  color: #8E8E8E;
  align-self: flex-start;
  margin-top: 1.3em;


  :nth-of-type(2n) {
    margin-top: 1em;
  }
`

export const Input = styled.input`
background: transparent;
  color: #8E8E8E;
  width: 100%;
  font-size: 1em;
  border: 2px solid #c9c9c9;
  border-radius: 5px;
  margin-top: 0.5em;
  box-sizing: border-box;
  padding: 0.8em;
  outline: none;

  ::placeholder {
    color: #c9c9c9;
  }
`;

export const Button = styled.button`
  background-color: #00AB0F;
  color: #ffffff;
  width: 100%;
  font-size: 1.2em;
  font-weight: 600;
  margin-top: 1em;
  padding: 0.5em;
  border: 2px solid #c9c9c9;
  border-radius: 5px;
  outline: none;
  cursor: pointer;
  transition: 0.2s;

  :hover {
      background-color: #027f0c;
  }

  :active {
    background-color: #00AB0F;
  }
`;