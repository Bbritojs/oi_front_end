import { useState } from 'react';
import {
    Button,
    Container,
    Input,
    InputContainer,
    Main,
    Text,
    Title,
    Logo,
    Label
} from './style'

function NewPassword() {
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setconfirmPassword] = useState("");


    return (
        <Container>
            <Main>
                <InputContainer>
                    <Logo sx={{ fontSize: '7em'}}/>
                    <Title>Troque sua senha!</Title>
                    <Text>
                        Crie sua senha com pelo menos 8 caracteres
                    </Text>
                    <Label>Nova senha: </Label>
                    <Input
                        value={newPassword}
                        onChange={(e) => setNewPassword(e.target.value)}
                        placeholder="Digite sua nova senha"
                        type="password"
                    />
                    <Label>Confirmação de nova senha:</Label>
                    <Input
                        value={confirmPassword}
                        onChange={(e) => setconfirmPassword(e.target.value)}
                        placeholder="Confirme sua nova senha"
                        type="password"
                    />
                    <Button>Redefinir senha</Button>
                </InputContainer>
            </Main>
        </Container>
    );
}

export default NewPassword;