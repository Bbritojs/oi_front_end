import * as React from 'react';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';
import TextField from '@mui/material/TextField/TextField';
import Box from '@mui/material/Box/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Consolidated from './consolidated';
import { makeStyles } from '@mui/styles';


const useStyles = makeStyles({
  divDetailed: {
    flexDirection: 'row',
    display: 'flex',
    margin: '5px',
    marginLeft: 90,

  },
  boxStyle: {
    flexDirection: 'row',
    display: 'flex',
    margin: '5px'
  }
})

export default function BasicDateTimePicker() {
  const classes = useStyles();
  const [value, setValue] = React.useState(new Date());
  const [age, setAge] = React.useState();
  const [status, setStatus] = React.useState();

  const handleChange = (e) => {
    setAge(e);
  };
  const handleStatus = (e) => {
    setStatus(e);
  };
  return (
    <>
      <Box sx={{m: 4, ml: 3,}}>
        <LocalizationProvider  dateAdapter={AdapterDateFns}>
          <DateTimePicker
            renderInput={(props) => <TextField {...props} />}
            label="DateTimePicker"
            value={value}
            onChange={(newValue) => {
              setValue(newValue);
            }}
          />
        </LocalizationProvider>
      </Box>

        <Box  sx={{ ml: 37, mt: -11, width: '10ch', }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Tipo</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={age}
              label="Tipo"
              onChange={handleChange}
            >
              <MenuItem value={10}>MT</MenuItem>
              <MenuItem value={20}>MO</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Box  sx={{ml: 51, mt: -7, width: '20ch', }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Status</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={status}
              label="Status"
              onChange={handleStatus}
            >
              <MenuItem value={10}>Sent</MenuItem>
              <MenuItem value={20}>Delivery Unknown</MenuItem>
              <MenuItem value={30}>Delivered</MenuItem>
              <MenuItem value={40}>Undelivered</MenuItem>
              <MenuItem value={50}>Failed</MenuItem>
            </Select>


          </FormControl>
        </Box>
        <Box  sx={{ml: 75, mt: -7, width: '20ch', }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Campanha</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={status}
              label="Campanha"
              onChange={handleStatus}
            >
              <MenuItem value={60}>Nome da campanha</MenuItem>
              <MenuItem value={70}>Nome da campanha</MenuItem>
            </Select>


          </FormControl>
        </Box>
        <Box  sx={{ml: 99, mt: -7, width: '20ch', }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Serviço</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={status}
              label="Serviço"
              onChange={handleStatus}
            >
              <MenuItem value={80}>Serviço 1</MenuItem>
              <MenuItem value={90}>Serviço 2</MenuItem>
            </Select>

          </FormControl>
        </Box>

        <Box sx={{ml: 123, mt: -7,  }}> 
         <TextField  id="outlined-basic" label="Numero Telefonico" variant="outlined" />
        </Box>

        <Box sx={{ml: 151, mt: -7,  }}> 
         <TextField id="outlined-basic" label="Short Code" variant="outlined" />          
        </Box>

        <Box sx={{ml: 179, mt: -7,  }}> 
         <TextField id="outlined-basic" label="ID da Mensagem" variant="outlined" />        
        </Box>
       
     
    </>
  );
}