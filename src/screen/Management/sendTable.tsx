import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


function createData(
    data: string,
    total: number,
    entregueMin: number,
    entregues: number,
    nentregues: number,
) {
    return { data, total, entregueMin, entregues, nentregues };
}

const rows = [
    createData('22/03/2022', 159, 100, 24, 4.0),
    createData('22/03/2022', 237, 50, 37, 4.3),
    createData('22/03/2022', 262, 102.0, 24, 6.0),
    createData('22/03/2022', 305, 123, 67, 4.3),
    createData('22/03/2022', 356, 290, 49, 0),
    createData('22/03/2022', 356, 350, 49, 0),
    createData('22/03/2022', 356, 40, 49, 0),
    createData('22/03/2022', 356, 350, 49, 0),
    createData('22/03/2022', 356, 350, 49, 0),
];




export default function SendTable() {
    return ( 
        <>       
        <TableContainer component={Paper} style={{ height: 552, width: '75%', }} sx={{ ml: 42, mt: 10, }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Data</TableCell>

                <TableCell align="right">Total de mensagens solicitadas</TableCell>
                <TableCell align="right">Entregues em até 15 min</TableCell>
                <TableCell align="right">Entregues entre 31 e 360 min</TableCell>
                <TableCell align="right">Não entregueMin em até 6 horas</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.data}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.data}
                  </TableCell>
                  <TableCell align="right">{row.total}</TableCell>
                  <TableCell align="right">{row.entregueMin}</TableCell>
                  <TableCell align="right">{row.entregues}</TableCell>
                  <TableCell align="right">{row.nentregues}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

      </>

    );
}
