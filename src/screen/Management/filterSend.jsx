import * as React from 'react';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';
import TextField from '@mui/material/TextField/TextField';
import Box from '@mui/material/Box/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Consolidated from './consolidated';
import { makeStyles } from '@mui/styles';


const useStyles = makeStyles({
  divDetailed: {
    flexDirection: 'row',
    display: 'flex',
    margin: '5px',
    marginLeft: 90,

  },
  boxStyle: {
    flexDirection: 'row',
    display: 'flex',
    margin: '5px'
  }
})

export default function BasicDateTimePicker() {
  const classes = useStyles();
  const [value, setValue] = React.useState(new Date());
  const [age, setAge] = React.useState();
  const [status, setStatus] = React.useState();

  const handleChange = (e) => {
    setAge(e);
  };
  const handleStatus = (e) => {
    setStatus(e);
  };
  return (
    <>


      <Box sx={{m: 4, ml: 3,}}>
      <LocalizationProvider  dateAdapter={AdapterDateFns}>
          <DateTimePicker
            renderInput={(props) => <TextField {...props} />}
            label="DateTimePicker"
            value={value}
            onChange={(newValue) => {
              setValue(newValue);
            }}
          />
        </LocalizationProvider>
       
        
      </Box>   
    </>
  );
}