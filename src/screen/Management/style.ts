import styled from "styled-components";

export const Container = styled.div`
  // display: flex;
  // flex-direction: column;
  align-items: center;
  justify-content: space-around;
  height: 87vh;
  color: #00000;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90vw;
  marginbottom:3000px
  margin-left: -2000px
`;

export const InputContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 60%;
`;

export const Input = styled.input`
  visibility: hidden;
  padding: 10px;
  font-size: 25px;

  width: 45%;

  outline: none;
  border: 2px solid #d9d9d9;

  ::placeholder {
    color: #c273fe;
  }
`;

export const Select = styled.select`
  visibility: hidden;
  padding: 10px;
  font-size: 25px;

  width: 20%;

  outline: none;
  border: 2px solid #d9d9d9;

  color: #52B75B;
`;

export const Option = styled.option``;

export const GraphContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 90vw;
  height: 60vh;
`;

export const TabContainer = styled.div`
  border-bottom: 2px solid #dbdbdb;
  width: 90vw;
`;

export const Tab = styled.button`
  cursor: pointer;
  margin-left: 20px;
  font-weight: bold;
  font-size: 1rem;
  background-color: #fff;
  outline: none;
  border: none;
  color: #52B75B;

  :nth-child(2) {
    margin-left: 100px;
  }

  :focus {
    color: #ca3af2;
  }
`;

export const TabMinor = styled(Tab)`
  cursor: pointer;
  font-size: 0.9rem;
  color: #bbb;

  :nth-child(2) {
    margin-left: 20px;
  }

  :focus {
    color: #000;
    border-bottom: 2px solid red;
  }
`;

export const FilterChartContainer = styled.div`
  border: 1px solid #dbdbdb;
  border-radius: 5px;
  width: 80%;
  height: 15%;
`;

export const FilterDescription = styled.p`
  border: 1px solid #dbdbdb;
  width: 80%;
  height: 15%;
`;

export const ChartsContent= styled.div`
  display: flex;
  justify-content: space-evenly;
  margin-top: 50px;
  width: 100%;
`;

export const ChartContainer = styled.div`
  display: flex;
  border: 1px solid #dbdbdb;
  border-radius: 5px;
  padding: 10px;
  width: 40%;
`;
