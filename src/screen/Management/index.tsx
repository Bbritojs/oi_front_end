import {
  Container,
  TitleContainer,
  InputContainer,
  Input,
  Select,
  Option,
  GraphContainer,
  TabContainer,
  TabMinor,
} from "./style";
import { brokerApi } from "../../service/http-out/brokerApi";
import React, { useEffect, useState, } from "react";
import jwtDecode from "jwt-decode";
import { useNavigate } from "react-router";
import Grid from '@mui/material/Grid';
import Consolidated from "./consolidated";
import Detailed from "./detailed";
import TableDetailed from "./tableDetailed";
import SendTable from './sendTable';
import FilterSend from './filterSend';
import ButtonCsv from "./buttonCsv";
import TableResponses from "./tableResponses";
import BasicDateRangePicker from "./datePicker";
import BlacklistTable from "./blacklistTable";



function Management() {
  const [messagesStatus, setMessagesStatus] = useState<any[]>([]);
  const [session, setSession] = useState("consolidado");
  const navigate = useNavigate();


  const optionsEnviadas = {
    responsive: true,
    plugins: {
      legend: {
        position: "bottom" as const,
      },
      title: {
        display: true,
        text: "Total de mensagens enviadas",
        position: "top" as const,
      },
    },
  };

  const optionsRecebidas = {
    responsive: true,
    plugins: {
      legend: {
        position: "bottom" as const,
      },
      title: {
        display: true,
        text: "Total de mensagens recebidas",
        position: "top" as const,
      },
    },
  };

  const optionsDelivery = {
    responsive: true,
    plugins: {
      legend: {
        position: "bottom" as const,
      },
      title: {
        display: true,
        text: "Delivery & Errors",
        position: "top" as const,
      },
    },
  };

  const optionsResponse = {
    responsive: true,
    plugins: {
      legend: {
        position: "bottom" as const,
      },
      title: {
        display: true,
        text: "Respostas",
        position: "top" as const,
      },
    },
  };


//index

  const labels = messagesStatus.map((element) => {
    return `${element.date}`;
  });




  function getData() {
    brokerApi
      .getDelivery()
      .then((res) => {
        setMessagesStatus(res.data);
        console.log(res.data);
      })
      .catch((e) => console.log(e.response.data));
  }

  useEffect(() => {
    const token = localStorage.getItem("token") || "";
    const decoded: any = jwtDecode(token);
    if (decoded.exp < Date.now() / 1000) {
      navigate("/");
    } else {
      getData();
    }
  }, [navigate]);



  return (
    <Container>
      <TitleContainer>
        <h1>Gerenciamento</h1>
        <InputContainer>
          <Input placeholder="Pequisar mensagens"></Input>
          <Select>
            <Option value="" disabled selected>
              Período
            </Option>
          </Select>
          <Select>
            <Option value="" disabled selected>
              Filtrar
            </Option>
          </Select>
        </InputContainer>
      </TitleContainer>
      <GraphContainer>
        <TabContainer>
          <TabMinor
            onClick={() => {
              setSession("consolidado");
            }}
          >
            Relatório Consolidado
          </TabMinor>
          <TabMinor
            onClick={() => {
              setSession("detalhado");
            }}
          >
            Relatório Detalhado
          </TabMinor>
          <TabMinor
            onClick={() => {
              setSession("envios");
            }}
          >
            Resumo de envios
          </TabMinor>
          <TabMinor
            onClick={() => {
              setSession("respostas");
            }}
          >
            Relatório de Respostas
          </TabMinor>
          <TabMinor
            onClick={() => {
              setSession("blacklist");
            }}
          >
            Blacklist
          </TabMinor>
        </TabContainer>
        <Grid>
          {session === "consolidado" ? (
            <>
              <Consolidated />

            </>
          ) : (
            <></>
          )}
          {session === "detalhado" ? (
            <>
              <Detailed />
              <TableDetailed />
              <ButtonCsv />
            </>
          ) : (
            <></>
          )}
          {session === "envios" ? (
            <>
              <BasicDateRangePicker />
              <SendTable />
              <ButtonCsv />
            </>
          ) : (
            <></>
          )}
          {session === "respostas" ? (
            <>
             <BasicDateRangePicker/>
             <TableResponses/>
             <ButtonCsv />



            </>
          ) : (
            <></>
          )}
          {session === "blacklist" ? (
            <>
            <BasicDateRangePicker/>
            <BlacklistTable/>
         

            </>
          ) : (
            <></>
          )}
        </Grid>
      </GraphContainer>
    </Container>
  );
}

export default Management;
