import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


function createData(
    numero: string,
    operadora: string,
    api: string,
    conteudo: number,
    status: string,
    erro: string,
) {
    return { numero, operadora, api, conteudo, status, erro, };
}

const rows = [
    createData('(11)98588-1100', 'OI', 'SIM', 24, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'NÃO', 37, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'SIM', 24, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'SIM', 67, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'NÃO', 49, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'NÃO', 49, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'NÃO', 49, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'SIM', 49, 'SEND', 'fail'),
    createData('(11)98588-1100', 'OI', 'SIM', 49, 'SEND', 'fail'),
];



export default function TableResponses() {
    return ( 
        <>       
        <TableContainer component={Paper} style={{ height: 552, width: '75%', }} sx={{ ml: 42, mt: 16, }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
             
              <TableRow>
                <TableCell>Número Telefônico</TableCell>
                <TableCell align="right">Ambiente Oi</TableCell>
                <TableCell align="right">API</TableCell>
                <TableCell align="right">Conteúdo</TableCell>
                <TableCell align="right">Status</TableCell>
                <TableCell align="right">Motivo do Erro</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.numero}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.numero}
                  </TableCell>
                  <TableCell align="right">{row.operadora}</TableCell>
                  <TableCell align="right">{row.api}</TableCell>
                  <TableCell align="right">{row.conteudo}</TableCell>
                  <TableCell align="right">{row.status}</TableCell>
                  <TableCell align="right">{row.erro}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

      </>

    );
}
