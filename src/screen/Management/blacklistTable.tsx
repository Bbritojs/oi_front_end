import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


function createData(
    data: string,
    total: number,
) {
    return { data, total};
}

const rows = [
    createData('22/03/2022 21:00', 11967806558),
    createData('22/03/2022 15:00', 11967806558),
    createData('22/03/2022 12:00', 11967806558),
    createData('22/03/2022 13:32', 11967806558),
    createData('22/03/2022 15:50', 11967806558),
    createData('22/03/2022 16:10', 11967806558),
    createData('22/03/2022 15:25', 11967806558),
    createData('22/03/2022 15:20', 11967806558),
    createData('22/03/2022 19:00', 11967806558),
];

const data = [
    {
        data: "01/Abr",
        enviadas: 4000,
        entregues: 2400,
        amt: 2400
    },
    {
        data: "02/Abr",
        enviadas: 3000,
        entregues: 1398,
        amt: 2210
    },
    {
        data: "03/Abr",
        enviadas: 2000,
        entregues: 9800,
        amt: 2290
    },
    {
        data: "04/Abr",
        enviadas: 780,
        entregues: 3008,
        amt: 2000
    },
    {
        data: "05/Abr",
        enviadas: 1890,
        entregues: 4800,
        amt: 2181
    },
    {
        data: "06/Abr",
        enviadas: 1000,
        entregues: 3800,
        amt: 2500
    },
    {
        data: "07/Abr",
        enviadas: 3490,
        entregues: 4300,
        amt: 2100
    }
];


export default function BlacklistTable() {
    return ( 
        <>       
        <TableContainer component={Paper} style={{ height: 552, width: '75%', }} sx={{ ml: 42, mt: 10, }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableCell>Relatório de porcentagem de entrega por dias</TableCell>
              <TableRow>
                <TableCell>Data e Hora</TableCell>

                <TableCell align="right">Telefone</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.data}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.data}
                  </TableCell>
                  <TableCell align="right">{row.total}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

      </>

    );
}
