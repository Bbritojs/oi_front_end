import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


function createData(
    data: string,
    total: number,
    enviadas: number,
    entregues: number,
    nentregues: number,
) {
    return { data, total, enviadas, entregues, nentregues };
}

const rows = [
    createData('22/03/2022', 159, 6.0, 24, 4.0),
    createData('22/03/2022', 237, 9.0, 37, 4.3),
    createData('22/03/2022', 262, 16.0, 24, 6.0),
    createData('22/03/2022', 305, 3.7, 67, 4.3),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
];




export default function TableDetailed() {
    return ( 
        <>       
        <TableContainer component={Paper} style={{ height: 552, width: '75%', }} sx={{ ml: 42, mt: 10, }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
       
              <TableRow>
                <TableCell>Data</TableCell>

                <TableCell align="right">Total</TableCell>
                <TableCell align="right">Enviadas</TableCell>
                <TableCell align="right">Entregues</TableCell>
                <TableCell align="right">Não Entregues</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.data}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.data}
                  </TableCell>
                  <TableCell align="right">{row.total}</TableCell>
                  <TableCell align="right">{row.enviadas}</TableCell>
                  <TableCell align="right">{row.entregues}</TableCell>
                  <TableCell align="right">{row.nentregues}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>

      </>

    );
}
