import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
} from "recharts";
import Grid from '@mui/material/Grid';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DetailedFilter from './filterConsolidated';

function createData(
    data: string,
    total: number,
    enviadas: number,
    entregues: number,
    nentregues: number,
) {
    return { data, total, enviadas, entregues, nentregues };
}

const rows = [
    createData('22/03/2022', 159, 6.0, 24, 4.0),
    createData('22/03/2022', 237, 9.0, 37, 4.3),
    createData('22/03/2022', 262, 16.0, 24, 6.0),
    createData('22/03/2022', 305, 3.7, 67, 4.3),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
    createData('22/03/2022', 356, 16.0, 49, 0),
];

const data = [
    {
        data: "01/Abr",
        enviadas: 4000,
        entregues: 2400,
        amt: 2400
    },
    {
        data: "02/Abr",
        enviadas: 3000,
        entregues: 1398,
        amt: 2210
    },
    {
        data: "03/Abr",
        enviadas: 2000,
        entregues: 9800,
        amt: 2290
    },
    {
        data: "04/Abr",
        enviadas: 2780,
        entregues: 3908,
        amt: 2000
    },
    {
        data: "05/Abr",
        enviadas: 1890,
        entregues: 4800,
        amt: 2181
    },
    {
        data: "06/Abr",
        enviadas: 2390,
        entregues: 3800,
        amt: 2500
    },
    {
        data: "07/Abr",
        enviadas: 3490,
        entregues: 4300,
        amt: 2100
    }
];


export default function Consolidated() {
    return ( 
        <>
        <DetailedFilter/>
        <Box sx={{ m: 3, ml: 240, }}>
          <Button variant="contained">Exportar .csv</Button>
        </Box>

        <Grid>
          <LineChart
            width={900}
            height={600}
            data={data}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="data" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="entregues"
              stroke="#8884d8"
              activeDot={{ r: 8 }}
            />
            <Line type="monotone" dataKey="enviadas" stroke="#82ca9d" />
          </LineChart>

        </Grid>

        <TableContainer component={Paper} style={{ height: 552, width: '55%', }} sx={{ ml: 122, mt: -75, }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableCell>Relatório de porcentagem de entrega por dias</TableCell>
              <TableRow>
                <TableCell>Data</TableCell>

                <TableCell align="right">Total</TableCell>
                <TableCell align="right">Enviadas</TableCell>
                <TableCell align="right">Entregues</TableCell>
                <TableCell align="right">Não Entregues</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.data}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.data}
                  </TableCell>
                  <TableCell align="right">{row.total}</TableCell>
                  <TableCell align="right">{row.enviadas}</TableCell>
                  <TableCell align="right">{row.entregues}</TableCell>
                  <TableCell align="right">{row.nentregues}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>



      </>

    );
}
