import { useState } from 'react';
import {
    Button,
    Container,
    Input,
    InputContainer,
    Main,
    Text,
    Title,
    Logo
} from './style'

function ForgotPassword() {
    const [email, setEmail] = useState("");

    return (
        <Container>
            <Main>
                <InputContainer>
                    <Logo sx={{ fontSize: '7em'}}/>
                    <Title>Problemas para acessar ?</Title>
                    <Text>
                        Insira o seu email, e enviaremos um link 
                        para você voltar a acessar a sua conta.
                    </Text>
                    <Input
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Digite seu email"
                        type="email"
                    />
                    <Button>Enviar link</Button>
                </InputContainer>
            </Main>
        </Container>
    );
}

export default ForgotPassword;