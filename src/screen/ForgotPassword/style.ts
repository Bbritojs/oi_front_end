import styled from "styled-components";
import LockOpenOutlinedIcon from '@mui/icons-material/LockOpenOutlined';

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    height: 100vh;

    background-color: #FAFAFA;
`

export const Main = styled.main`
    display: flex;
    justify-content: center;

    width: 25%;
    height: 63%;
    max-height: 400px;


    padding: 2em 1.5em;
    /* border: 1px solid #a5a5a5; */
    border-radius: 0.5em;

    box-shadow: rgba(99, 99, 99, 0.4) 0px 2px 8px 0px;
`

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  /* justify-content: space-around; */

  width: 80%;


  span {
    color: #fff;
    font-size: 1.3rem;
    cursor: pointer;
  }
`;

export const Logo = styled(LockOpenOutlinedIcon)``

export const Title = styled.h3`
    color: #000;
`

export const Text = styled.p`
    color: #8E8E8E;
    text-align: center;

`

export const Input = styled.input`
background: transparent;
  color: #8E8E8E;
  width: 100%;
  font-size: 1em;
  border: 2px solid #c9c9c9;
  border-radius: 5px;
  margin-top: 1.5em;
  box-sizing: border-box;
  padding: 0.8em;
  outline: none;

  ::placeholder {
    color: #c9c9c9;
  }
`;

export const Button = styled.button`
  background-color: #00AB0F;
  color: #ffffff;
  width: 100%;
  font-size: 1.2em;
  font-weight: 600;
  margin-top: 1em;
  padding: 0.5em;
  border: 2px solid #c9c9c9;
  border-radius: 5px;
  outline: none;
  cursor: pointer;
  transition: 0.2s;

  :hover {
      background-color: #027f0c;
  }

  :active {
    background-color: #00AB0F;
  }
`;