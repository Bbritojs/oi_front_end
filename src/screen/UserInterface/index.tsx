import { brokerApi } from "../../service/http-out/brokerApi";
import jwtDecode from "jwt-decode";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { User } from "./domain/user";
import {
  Container,
  Title,
  InputContainer,
  Input,
  LocalButton as Button,
} from "./style";
//import { Container, Row, Col } from 'react-bootstrap'

function UserCreate() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const user: User = { name: "", email: "", type: "", password: ""};

  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token') || "";
      const decoded: any = jwtDecode(token);
      if (decoded.exp < Date.now() / 1000) {
        navigate("/");
      }
  });

  async function handleClick() {
    user.name = name; 
    user.email = email;
    user.password = password;
    user.type = "admin";

    // setLoading(true);
    await brokerApi
      .createUser(user)
      .then((response) => {
        console.log("resp ", response.data);
        reset();
        alert("Usuário criada com sucesso")
      })
      .catch((error) => {
        console.log(error.response);
        alert("Ocorreu um erro ao criar usuário");
      });
    // setLoading(false);
  }

  function reset() {
    setName("")
    setEmail("")
    setPassword("")
  }

  return (
    <Container>
      <Title>Cadastro de Usuário</Title>
      <InputContainer autoComplete="off">
        <Input 
        value={name} 
        onChange={(e) => setName(e.target.value)}
        type="text" placeholder="Nome de usuário"></Input>
        <Input 
        value={email} 
        onChange={(e) => setEmail(e.target.value)}
        type="text" placeholder="E-mail"></Input>
        <Input 
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        type="password" placeholder="Senha"></Input>
        <Button onClick={handleClick}>Criar Usuário</Button>
      </InputContainer>
    </Container>
  );
}

export default UserCreate;
