import styled from "styled-components";
import { Button } from '../../styles/global'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 95%;
  height: 80vh;
  padding: 2rem 0 0 2rem;
`;

export const Title = styled.h1`
  color: #black;
`;

export const InputContainer = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;

  width: 90vw;
  height: 70%;
`;

export const Input = styled.input`
  padding: 10px;
  font-size: 25px;

  width: 35%;

  outline: none;
  border: 2px solid #black;
  border-radius: 5px;

  ::placeholder {
    color: #000000;
  }
`;

export const Label = styled.label`
  display: flex;
  align-items: center;
  justify-content: space-around;

  width: 20%;
  padding: 10px;

  font-size: 25px;
  border: 2px solid #d9d9d9;
  color: #52B75B;
  cursor: pointer;
`;

export const InputFile = styled.input.attrs({ type: "file" })`
  display: none;
`;

export const LocalButton = styled(Button)`
  width: 17%;
  height: 15%;
  color:white
`;
