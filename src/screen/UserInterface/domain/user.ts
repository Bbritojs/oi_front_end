export interface User {
    name: string;
    email: string;
    type: string;
    password: string;
  }
  