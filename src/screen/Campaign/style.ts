import styled from "styled-components";
import { Button } from "../../styles/global";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  height: 87vh;
  color: #52b75b;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90vw;
`;

export const LocalButton = styled(Button)`
  width: 17%;
  height: 60%;
`;

export const InputContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 90vw;
`;

export const Block = styled.div`
  display: flex;
  flex-direction: column;
`

export const Input = styled.input`
  padding: 10px;
  font-size: 25px;

  width: 100%;

  outline: none;
  border: 2px solid #d9d9d9;

  ::placeholder {
    color: #52b75b;
  }
`;

export const Select = styled.select`
  padding: 10px;
  font-size: 25px;

  width: 100%;

  outline: none;
  border: 2px solid #d9d9d9;

  color: #52b75b;
`;

export const Option = styled.option`
`;

export const TableContainer = styled.div`
  overflow-y: scroll;
  width: 90vw;
  height: 60vh;
  ::-webkit-scrollbar {
    width: 10px;
  }
  
  ::-webkit-scrollbar-track {
    background: #f1f1f1; 
  }
   
  ::-webkit-scrollbar-thumb {
    background: #888; 
  }
  
  ::-webkit-scrollbar-thumb:hover {
    background: #555; 
  }
`;

export const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
  font-size: 17px;
  font-weight: bold;
`;

export const TableRow = styled.tr``;

export const TableHeader = styled.thead`
  border-bottom: 2px solid #dbdbdb;
`;

export const TableHead = styled.th`
  padding: 1rem;
  text-align: left;
  text-align: center;

  :first-child {
    text-align: start;
  }
`;

export const TableBody = styled.tbody`
overflow-y: scroll;
`;

export const TableData = styled.td`
  padding: 1rem;
  text-align: left;
  text-align: center;
  border-bottom: 2px solid #dbdbdb;

  :first-child {
    text-align: start;
  }
`;
