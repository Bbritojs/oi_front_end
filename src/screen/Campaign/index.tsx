import {
  Container,
  LocalButton as Button,
  TitleContainer,
  InputContainer,
  Input,
  Select,
  Option,
  TableContainer,
  Table,
  TableRow,
  TableHeader,
  TableHead,
  TableBody,
  TableData,
  Block
} from "./style";
import { useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { brokerApi } from "../../service/http-out/brokerApi";
import jwtDecode from "jwt-decode";

function Campaing() {
  const navigate = useNavigate();
  const [campaigns, setCampaigns] = useState<any>([]);
  const [originalCampaigns, setOriginalCampaigns] = useState<any>([]);

  const [name, setName] = useState("");
  

  function createCampaing() {
    navigate("/campaign-create");
  }

  useEffect(() => {
    const token = localStorage.getItem('token') || "";
    const decoded: any = jwtDecode(token);
    if (decoded.exp < Date.now() / 1000) {
      navigate("/");
    }
  })

  useEffect(() => {
    brokerApi
      .getCampaign()
      .then((res) => {
        setCampaigns(res.data);
        setOriginalCampaigns(res.data);
      })
      .catch((e) => console.log(e.response.data));
  }, []);

  useEffect(() => {
    const filterData = [];
    if (name){
      for (let i = 0; i < originalCampaigns.length; i++){
        const value = originalCampaigns[i].name.toLowerCase()
        if(value.includes(name.toLowerCase())) {
          filterData.push(originalCampaigns[i])
        }
      }
      if ( filterData.length === 0 ){
        setCampaigns(originalCampaigns);
      } else {
        setCampaigns(filterData); 
      }
    } else {
      setCampaigns(originalCampaigns);
    }
  },[name, originalCampaigns])

  function find(value: any) {
    if(value === '0'){
      setCampaigns(originalCampaigns)
    }
    else if(value === '1') {
      const filterData = [];
      const today = new Date()
      const dayToday = today.getDate()
      const monthToday = today.getMonth()
      const yearToday = today.getFullYear()
      for (let i = 0; i < originalCampaigns.length; i++){
        const value = new Date(originalCampaigns[i].date)
        const dayValue = value.getDate() + 1
        const monthValue = value.getMonth()
        const yearValue= value.getFullYear()
        if(dayToday === dayValue && monthToday === monthValue && yearToday === yearValue) {
          filterData.push(originalCampaigns[i])
        }
      }
      setCampaigns(filterData);
    } else if( value === '2') {
      const filterData = [];
      const today = new Date()
      const oneWeekAgo = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7)
      for (let i = 0; i < originalCampaigns.length; i++){
        const value = new Date(originalCampaigns[i].date)
        if(oneWeekAgo < value) {
          filterData.push(originalCampaigns[i])
        }
      }
      setCampaigns(filterData);
    } else if( value === '3') {
      const filterData = [];
      const today = new Date()
      const monthToday = today.getMonth()
      const yearToday = today.getFullYear()
      for (let i = 0; i < originalCampaigns.length; i++){
        const value = new Date(originalCampaigns[i].date)
        const monthValue = value.getMonth()
        const yearValue= value.getFullYear()
        if(monthToday === monthValue && yearToday === yearValue) {
          filterData.push(originalCampaigns[i])
        }
      }
      setCampaigns(filterData);
    }
  }  

  async function orderBy(type: any) {
    const order = campaigns;
    find("1");
    if (type === '2') {
      const promise = new Promise((resolve) => {
        resolve (order.sort((a: any, b: any) => {
          return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : a.name > b.name ? 1 : 0;
        }))
      });
      
      promise.then((value) => {
        console.log(value)
        setCampaigns(value);
      })
      
    } else {
      const promise = new Promise((resolve) => {
        resolve (order.sort((a: any, b: any) => {
          return a.date < b.date ? -1 : a.date > b.date ? 1 : 0;
        }))
      })

      promise.then((value) => {
        console.log(value)
        setCampaigns(value);
      })
    }
  }
  return (
    <Container>
      <TitleContainer>
        <h1>CAMPANHAS</h1>
        <Button onClick={createCampaing}>Criar Campanha</Button>
      </TitleContainer>

      <InputContainer>
      <Block>
        <h3>Pesquisar</h3>
        <Input  
        value={name}
        onChange={(e) => setName(e.target.value)}
        placeholder="Nome das Campanhas" />
      </Block>
      <Block>
        <h3>Filtrar</h3>
        <Select onChange={(e) => {find(e.target.value)}}>
          <Option value="" disabled selected>
            Periodo
          </Option>
          <Option value="0">Todos</Option>
          <Option value="1">Hoje</Option>
          <Option value="2">Esta semana</Option>
          <Option value="3">Este mes</Option>
        </Select>
      </Block>
      <Block>
        <h3>Ordernar por:</h3>
        <Select onChange={(e) => {orderBy(e.target.value)}}>
          <Option value="1">Data de envio</Option>
          <Option value="2">Ordem alfabética</Option>
        </Select>
      </Block>
      </InputContainer>

      <TableContainer>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHead>NOME DA CAMPANHA</TableHead>
              <TableHead>DATA DE ENVIO</TableHead>
              <TableHead>CONTATOS SELECIONADOS</TableHead>
              <TableHead>TIPO</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {campaigns.map((campaign: any) => (
              <TableRow key={campaign.id}>
                <TableData>{campaign.name}</TableData>
                <TableData>{new Date(campaign.date).toLocaleDateString('pt-BR', {timeZone: 'UTC'})}</TableData>
                <TableData>{campaign.contactsAmount}</TableData>
                <TableData>{campaign.type}</TableData>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}

export default Campaing;
