import styled from "styled-components";
import Snackbar from '@mui/material/Snackbar';


export const Container = styled.div`
  display: flex;
`;

export const FirstSection = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: #fff;

  width: 50vw;
  height: 100vh;
`;

export const Logo = styled.img`
  height: 30vw;
  width: 35vw;
`;

export const SecondSection = styled(FirstSection)`
  background-color: #00AB0F;
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;

  width: 70%;
  height: 60%;
  max-height: 400px;
`;

export const Input = styled.input`
background: transparent;
  color: #fff;
  width: 80%;
  font-size: 1.5em;
  border: 2px solid #fff;
  border-radius: 5px;
  padding: 10px;
  outline: none;

  ::placeholder {
    color: #fff;
  }
`;

export const Button = styled.button`
  background-color:  #026d0b;
  color: #ffffff;
  font-size: 1.3em;
  font-weight: 600;
  padding: 0.5em 1em;
  border: 2px solid #fff;
  border-radius: 5px;
  outline: none;
  cursor: pointer;
  transition: 0.2s;
`;

export const Link = styled.span`
  color: white;
  font-size: 1.2em;
  text-decoration: none;
  cursor: pointer;
  transition: 0.3s;

  :hover {
    color: #000;
  };

  :active {
    color: #fff;
  }
`

export const Notification = styled(Snackbar)``