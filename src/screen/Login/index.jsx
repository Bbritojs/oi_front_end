import { brokerApi } from "../../service/http-out/brokerApi";
import Alert from '@mui/material/Alert';
import {
  Container,
  FirstSection,
  SecondSection,
  Logo,
  InputContainer,
  Input,
  Button,
  Link,
  Notification
} from "./style";
import logo from "../../assets/oi-logo.png";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Login() {
  const [open, setOpen] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  function showAlert() {
    setOpen(true)
  }

  const closeAlert = () => {
    setOpen(false);
  };

  async function login() {
    try {
      const response = await brokerApi.login(email, password);

      localStorage.setItem('token', response.data.token)
      localStorage.setItem('id', response.data.id)
      localStorage.setItem('name', response.data.name)

      navigate("/campaign-create");

    } catch (e) {
      showAlert();
      console.log(e.response.status);
    }
  }

  function forgotPassword() {
    navigate("/forgot-password");
  }

  return (
    <Container>
      <Notification
        open={open}
        onClose={closeAlert}
        autoHideDuration={3000}
      >
        <Alert
          onClose={closeAlert}
          severity="error"
          variant="filled"
        >
          Usuário ou Senha está incorreto!
        </Alert>
      </Notification>
      <FirstSection>
        <Logo src={logo} alt="" />
      </FirstSection>
      <SecondSection>
        <InputContainer>
          <Input
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Usuário"
            type="email"
          ></Input>
          <Input
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Senha"
            type="password"
          ></Input>
          <Button onClick={login}>Entrar</Button>
          <Link onClick={forgotPassword}>Esqueci minha senha</Link>
        </InputContainer>
      </SecondSection>
    </Container>
  );
}

export default Login;
