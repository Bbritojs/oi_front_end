import {
  Container,
  LocalButton as Button,
  TitleContainer,
  InputContainer,
  // SelectCategoria,

} from "./style";
import React, { useEffect, useState } from "react";
import { brokerApi } from "../../service/http-out/brokerApi";
import { useNavigate } from "react-router";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { DatePicker, LocalizationProvider } from '@mui/lab'
import { TextField } from '@mui/material'
import brLocale from 'date-fns/locale/pt-BR'
import jwtDecode from "jwt-decode";

function Logs() {
  const navigate = useNavigate();
  const [fromDate, setFromDate] = useState<any>(new Date())
  const [toDate, setToDate] = useState<any>(new Date())
  const [minDate] = useState<any>(new Date().setMonth(new Date().getMonth() - 6))
  const [maxDate] = useState<any>(new Date())

  useEffect(() => {
    const token = localStorage.getItem("token") || "";
    const decoded: any = jwtDecode(token);
    if (decoded.exp < Date.now() / 1000) {
      navigate("/");
    }
  });

  function exportcsv() {
    const day = fromDate.getDate()
    const month = fromDate.getMonth() + 1
    const year = fromDate.getFullYear()
    const dateFormatFrom = `${year}-${month < 10 ? '0'+month : month}-${day < 10 ? '0'+day : day}`

    const dayTo = toDate.getDate()
    const monthTo = toDate.getMonth() + 1
    const yearTo = toDate.getFullYear()
    const dateFormatTo = `${yearTo}-${monthTo < 10 ? '0'+monthTo : monthTo}-${dayTo < 10 ? '0'+dayTo : dayTo}`

    const myDate = {fromDate : `${dateFormatFrom}`, toDate : `${dateFormatTo}`};
    brokerApi
        .getWebhookStatusCsvExport(myDate)
        .then((res) => {
          console.log(res.data);
          window.open(res.data);
        })
        .catch((e) => console.log('err: ', e.response.data));
        
      }
      
      return (
        <Container>
     
      <TitleContainer>
        <h1>Controle de mensagens SMS</h1>
      </TitleContainer>
          {/* <h4  style={{marginBottom: "-30px",marginRight:"148px"}}>Categoria</h4> */}

      <InputContainer>
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={brLocale}>
          <DatePicker 
            label='Data início'
            minDate={minDate}
            value={fromDate}
            onChange={(date) => {setFromDate(date)}}
            renderInput={(params) => (<TextField {...params}/>)}
          />
        </LocalizationProvider>
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={brLocale}>
          <DatePicker
              label='Data fim'
              maxDate={maxDate}
              value={toDate}
              onChange={(date) => {setToDate(date)}}
              renderInput={(params) => (<TextField {...params}/>)}
          />
        </LocalizationProvider>

        <select style={{marginInlineStart: "90px",paddingBottom:"34px",fontSize:"25px",paddingLeft: "17px",textAlign:"center",cursor:"pointer"}} id="s1" name="s1"  >
            <option style={{cursor:"pointer"}}>28591 - Marketing</option>
            <option style={{cursor:"pointer"}}>28446 - Serviço</option>
            <option>Marketing</option>
        </select>
        
        <Button onClick={() => {exportcsv()}}>Exportar .csv</Button>
      
      </InputContainer>
    </Container>
    
  );
}

export default Logs;
