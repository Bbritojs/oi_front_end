import styled from "styled-components";
import { Button } from "../../styles/global";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  // height: 7vh;
  width: 100vw;
  color: #fffff;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90vw;
`;

export const LocalButton = styled(Button)`
  width: 20%;
  height: 60%;
  color:#ffffff;
  
`;

export const InputContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 90vw;
  margin: -60px

`;

export const Input = styled.input`
  padding: 10px;
  font-size: 25px;

  width: 30%;

  outline: none;
  border: 2px solid #d9d9d9;

  ::placeholder {
    color: #fffff;
  }
`;

export const Select = styled.select`
  padding: 10px;
  font-size: 25px;

  width: 15%;

  outline: none;
  border: 2px solid #d9d9d9;

  color: #fffff;
`;

export const Option = styled.option`
`;

export const TableContainer = styled.div`
  width: 90vw;
  height: 60vh;
`;

export const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
  font-size: 17px;
  font-weight: bold;
`;

export const TableRow = styled.tr``;

export const TableHeader = styled.thead`
  border-bottom: 2px solid #dbdbdb;
`;

export const TableHead = styled.th`
  padding: 1rem;
  text-align: left;
  text-align: center;

  :first-child {
    text-align: start;
  }
`;

export const TableBody = styled.tbody``;

export const TableData = styled.td`
  padding: 1rem;
  text-align: left;
  text-align: center;
  border-bottom: 2px solid #dbdbdb;

  :first-child {
    text-align: start;
  }
`;
