import {
  Container,
  LocalButton as Button,
  TitleContainer,
  InputContainer,
  Input,
  TableContainer,
  Table,
  TableRow,
  TableHeader,
  TableHead,
  TableBody,
  TableData,
} from "./style";
// import { useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { brokerApi } from "../../service/http-out/brokerApi";
import { useNavigate } from "react-router";
import jwtDecode from "jwt-decode";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { DatePicker, LocalizationProvider } from '@mui/lab'
import { TextField } from '@mui/material'
import brLocale from 'date-fns/locale/pt-BR'
// import { useNavigate } from "react-router-dom";


function BlackList() {
  const navigate = useNavigate();
  const [date, setDate] = useState<any>(new Date())
  const [campaigns, setCampaigns] = useState<any>([]);
  const [originalCampaigns, setOriginalCampaigns] = useState<any>([]);

  
  useEffect(() => {
    const token = localStorage.getItem('token') || "";
    const decoded: any = jwtDecode(token);
    if (decoded.exp < Date.now() / 1000) {
      navigate("/");
    }
  })

  function exportcsv() {
    let csvContent = "data:text/csv;charset=utf-8,";

    // eslint-disable-next-line array-callback-return
    campaigns.map((element: any) => {
      csvContent += element.name + "," + element.date + "," + JSON.stringify(element.blacklist) + "\r\n";
    })

    csvContent = csvContent.replaceAll('{"number":', '')
    csvContent = csvContent.replaceAll('}', '')

    const encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
  }

  useEffect(() => {
    const day = date.getDate()
    const month = date.getMonth() + 1
    const year = date.getFullYear()
    const dateFormat = `${year}-${month < 10 ? '0'+month : month}-${day < 10 ? '0'+day : day}`
    brokerApi
      .getCampaignBlackList(dateFormat)
      .then((res) => {
        setCampaigns(res.data);
        setOriginalCampaigns(res.data)
      })
      .catch((e) => console.log(e.response.data));
  }, [date]);

  function findName(name: any) {
    const filterData = [];
    if (name){
      for (let i = 0; i < originalCampaigns.length; i++){
        name = name.toLowerCase()
        const value = originalCampaigns[i].name.toLowerCase();
        if(value.includes(name)) {
          filterData.push(originalCampaigns[i])
        }
      }
      if ( filterData.length === 0 ){
        setCampaigns(originalCampaigns);
      } else {
        setCampaigns(filterData); 
      }
    } else {
      setCampaigns(originalCampaigns);
    }
  } 

  return (
    <Container>
      <TitleContainer>
        <h1>Black List</h1>
      </TitleContainer>

      <InputContainer>
        <Button onClick={() => {exportcsv()}}>Exportar .csv</Button>

      </InputContainer>

      {/*<TableContainer> */}
      {/*  <Table>*/}
      {/*    <TableHeader>*/}
      {/*      <TableRow>*/}
      {/*        <TableHead>NOME DA CAMPANHA</TableHead>*/}
      {/*        <TableHead>DATA DE ENVIO</TableHead>*/}
      {/*      </TableRow>*/}
      {/*    </TableHeader>*/}
      {/*    <TableBody>*/}
      {/*      {campaigns.map((campaign: any) => (*/}
      {/*        <TableRow key={campaign.id}>*/}
      {/*          <TableData>{campaign.name}</TableData>*/}
      {/*          <TableData>{new Date(campaign.date).toLocaleDateString('pt-BR', { timeZone: 'UTC' })}</TableData>*/}
      {/*        </TableRow>*/}
      {/*      ))}*/}
      {/*    </TableBody>*/}
      {/*  </Table>*/}
      {/*</TableContainer>*/}
    </Container>
  );
}

export default BlackList;
