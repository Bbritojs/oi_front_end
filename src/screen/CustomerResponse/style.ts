import styled from "styled-components";
import { Button } from "../../styles/global";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  height: 33vh;
  color: #00000;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 90vw;
`;

export const SelectCategoria = styled.div`
display:flex;
flex-direction: column;
// height: 300px;
font-size: 15px;
width: 20vw;
// padding: 100px;
float: left

select{
  pading:20px
}

`;

export const LocalButton = styled(Button)`
  width: 17%;
`;

export const InputContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 90vw;
`;

export const Input = styled.input`
  padding: 10px;
  font-size: 25px;

  width: 30%;

  outline: none;
  border: 2px solid #d9d9d9;

  ::placeholder {
    color: #4f759a;
  }
`;



export const Select = styled.select`
  padding: 10px;
  font-size: 25px;

  width: 150%;

  outline: none;
  border: 2px solid #d9d9d9;

  color: #00000;
`;

export const Option = styled.option`
`;

export const TableContainer = styled.div`
  overflow-y: scroll;
  width: 90vw;
  height: 60vh;
  ::-webkit-scrollbar {
    width: 10px;
  }
  
  ::-webkit-scrollbar-track {
    background: #f1f1f1; 
  }
   
  ::-webkit-scrollbar-thumb {
    background: #888; 
  }
  
  ::-webkit-scrollbar-thumb:hover {
    background: #555; 
  }
`;

export const Table = styled.table`
  border-collapse: collapse;
  width: 100%;
  font-size: 17px;
  font-weight: bold;
`;

export const TableRow = styled.tr``;

export const TableHeader = styled.thead`
  border-bottom: 2px solid #dbdbdb;
`;

export const TableHead = styled.th`
  padding: 1rem;
  text-align: left;
  text-align: center;

  :first-child {
    text-align: start;
  }
`;

export const TableBody = styled.tbody`
overflow-y: scroll;

`;

export const TableData = styled.td`
  padding: 1rem;
  text-align: left;
  text-align: center;
  border-bottom: 2px solid #dbdbdb;

  :first-child {
    text-align: start;
    width: 20%;
  }

  :nth-child(2) {
    width: 20%;
  }
`;
