import {
  Container,
  LocalButton as Button,
  TitleContainer,
  InputContainer,
  SelectCategoria,

} from "./style";
import React, { useEffect, useState } from "react";
import { brokerApi } from "../../service/http-out/brokerApi";
import { useNavigate } from "react-router";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { DatePicker, LocalizationProvider } from '@mui/lab'
import { TextField } from '@mui/material'
import brLocale from 'date-fns/locale/pt-BR'
import jwtDecode from "jwt-decode";

function CustomerResponse() {
  const navigate = useNavigate();
  const [fromDate, setFromDate] = useState<any>(new Date())
  const [toDate, setToDate] = useState<any>(new Date())

  useEffect(() => {
    const token = localStorage.getItem("token") || "";
    const decoded: any = jwtDecode(token);
    if (decoded.exp < Date.now() / 1000) {
      navigate("/");
    }
  });

  function exportcsv() {
    const day = fromDate.getDate()
    const month = fromDate.getMonth() + 1
    const year = fromDate.getFullYear()
    const dateFormatFrom = `${year}-${month < 10 ? '0'+month : month}-${day < 10 ? '0'+day : day}`

    const dayTo = toDate.getDate()
    const monthTo = toDate.getMonth() + 1
    const yearTo = toDate.getFullYear()
    const dateFormatTo = `${yearTo}-${monthTo < 10 ? '0'+monthTo : monthTo}-${dayTo < 10 ? '0'+dayTo : dayTo}`

    const myDate = {fromDate : `${dateFormatFrom}`, toDate : `${dateFormatTo}`};
    brokerApi
        .getSmsReceivedCsvExport(myDate)
        .then((res) => {
          console.log(res.data);
          window.open(res.data);
        })
        .catch((e) => console.log('err: ', e.response.data));
        
      }
      
      return (
        <Container>
     
      <TitleContainer>
        <h1>Controle de respostas SMS</h1>
      </TitleContainer>

      <InputContainer>
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={brLocale}>
          <DatePicker 
            label='Data início'
            value={fromDate}
            onChange={(date) => {setFromDate(date)}}
            renderInput={(params) => (<TextField {...params}/>)}
          />
        </LocalizationProvider>
        <LocalizationProvider dateAdapter={AdapterDateFns} locale={brLocale}>
          <DatePicker
              label='Data fim'
              value={toDate}
              onChange={(date) => {setToDate(date)}}
              renderInput={(params) => (<TextField {...params}/>)}
          />
        </LocalizationProvider>
        
        <Button onClick={() => {exportcsv()}}>Exportar .csv</Button>
      
      </InputContainer>
    </Container>
    
  );
}

export default CustomerResponse;
